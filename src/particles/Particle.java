/**
 *
 */
package particles;

import javafx.scene.paint.Color;

import java.util.Random;

public class Particle implements Runnable {

    private ParticlesController controller;

    // taille (largeur et hauteur) de la particule
    private static final double SIZE = 10.0;

    // position de la particule (initialement : centre du panneau)
    private double x, y;

    // couleur de la particule (aléatoire)
    private Color color;

    // vitesse de la particule (aléatoire, entre -SIZE et SIZE)
    private double vx, vy;

    /**
     * constructeur
     *
     * @param controller contrôleur de l'application
     */
    public Particle(ParticlesController controller) {
        this.controller = controller;
        this.x = (controller.getCanvas().getWidth() / 2);
        this.y = (controller.getCanvas().getHeight() / 2);
        Random random = new Random();
        this.color = Color.color(random.nextDouble(), random.nextDouble(), random.nextDouble());
        this.vx = -(random.nextDouble() * Particle.SIZE) + (random.nextDouble() * Particle.SIZE);
        this.vy = -(random.nextDouble() * Particle.SIZE) + (random.nextDouble() * Particle.SIZE);
    }

    /**
     * dessine la particule sur le Canvas du controleur.
     * Cette méthode sera invoquée par la méthode drawParticles du controleur
     */
    public void draw() {
        this.controller.getCanvas().getGraphicsContext2D().setFill(this.color);
        this.controller.getCanvas().getGraphicsContext2D().fillOval(x, y, Particle.SIZE, Particle.SIZE);
    }

    /**
     * met à jour la position de la particule
     */
    public void update() {
        synchronized (this.controller.getParticles()) {
            for (Particle particle : this.controller.getParticles()
                    ) {
                this.collisionTest(particle);

            }
        }

        if ((this.x + this.vx) >= (this.controller.getCanvas().getWidth()) - Particle.SIZE) {
            this.x = this.controller.getCanvas().getWidth() - Particle.SIZE;
            this.vx *= -1;
        } else if ((this.x + this.vx) < 0) {
            this.x = 0;
            this.vx *= -1;
        } else {
            this.x += this.vx;
        }

        if ((this.y + this.vy) >= (this.controller.getCanvas().getHeight()) - Particle.SIZE) {
            this.y = this.controller.getCanvas().getHeight() - Particle.SIZE;
            this.vy *= -1;
        } else if ((this.y + this.vy) < 0) {
            this.y = 0;
            this.vy *= -1;
        } else {
            this.y += this.vy;
        }
    }

    /**
     * retourne la visibilité de la particule (selon sa position, sa taille et
     * celle du canvas)
     *
     * @return true si la particule est visible, false sinon
     */
    public boolean isVisible() {
        return (this.x) <= (this.controller.getCanvas().getWidth() + Particle.SIZE) &&
                (this.y) <= (this.controller.getCanvas().getHeight() + Particle.SIZE) &&
                (this.y) >= -Particle.SIZE &&
                (this.x) >= -Particle.SIZE;
    }

    /**
     * animation de la particule :
     * <p>
     * Tant que la particule est visible
     * - mise Ã  jour de la particule (méthode update)
     * - déclencher le dessin de toutes les particules
     * - "dormir" pendant 25 ms (Thread.sleep)
     * <p>
     * Lorsque la particule n'est plus visible :
     * - retirer la particule du contrôleur
     * - ajouter une nouvelle particule au contrôleur
     */
    public void run() {
        while (this.isVisible()) {
            this.update();
            this.controller.drawParticles();
            try {
                Thread.sleep(25);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        this.controller.removeParticle(this);
        this.controller.addParticle();
    }


    /**
     * effectue un test de collision entre la particule courante et
     * la particule p. S'il y a collision, les vitesses des particules sont
     * recalculées en fonction des vitesses initiales.
     *
     * @param p particule à tester
     */
    private void collisionTest(Particle p) {
        if (p == this) {
            return;
        }
        double nx = x - p.x;
        double ny = y - p.y;
        double d = Math.sqrt(nx * nx + ny * ny);
        if (d <= SIZE && d > 0.0001) {
            nx = nx * (SIZE - d) / d;
            ny = ny * (SIZE - d) / d;
            x += nx * 0.5;
            y += ny * 0.5;
            p.x -= nx * 0.5;
            p.y -= ny * 0.5;
            d = Math.sqrt(nx * nx + ny * ny);
            nx /= d;
            ny /= d;
            double vn = (vx - p.vx) * nx + (vy - p.vy) * ny;
            if (vn > 0.0) {
                return;
            }
            vn *= -0.9;
            nx *= vn;
            ny *= vn;
            vx += nx;
            vy += ny;
            p.vx -= nx;
            p.vy -= ny;
        }
    }

}
