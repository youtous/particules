package particles;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.Label;
import javafx.scene.paint.Color;

import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ParticlesController {
    @FXML
    private Canvas canvas;

    @FXML
    private Label label;

    private ArrayList<Particle> particles = new ArrayList<>();

    private ExecutorService pool = Executors.newCachedThreadPool();

    /**
     * retourne le canvas dans lequel les particules doivent être dessinées
     *
     * @return the canvas
     */
    public Canvas getCanvas() {
        return this.canvas;
    }

    /**
     * ajoute une nouvelle particule (addParticle) à  la liste et met à  jour le texte du label.
     */
    @FXML
    public void onAdd() {
        this.addParticle();
        this.label.setText(particles.size() + " particules");
        this.drawParticles();
    }

    /**
     * ajoute une nouvelle particule à la liste et la soumet au pool
     */
    public void addParticle() {
        synchronized (this.particles) {
            Particle particle = new Particle(this);
            this.particles.add(particle);
            this.pool.submit(particle);
        }
    }

    /**
     * retire une particule de la liste
     *
     * @param p particule à  retirer
     */
    public void removeParticle(Particle p) {
        synchronized (this.particles) {
            this.particles.remove(p);
        }
    }

    /**
     * efface le canvas (dessin d'un rectangle noir) et dessine les particules de la liste (par appel de leur méthode draw).
     */
    public void drawParticles() {
        Platform.runLater(() -> {
            this.canvas.getGraphicsContext2D().setFill(Color.BLACK);
            this.canvas.getGraphicsContext2D().fillRect(0, 0, this.canvas.getWidth(), this.canvas.getHeight());

            synchronized (this.particles) {
                for (Particle particle : this.particles) {
                    particle.draw();
                }
            }

        });
    }

    public ArrayList<Particle> getParticles() {
        return particles;
    }
}